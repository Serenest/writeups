<h1> Farmers Only </h1>

**Category**: miscalleneous

**Points**: 990

**Author**: balex

<h2> TL;DR </h2>

* Shanks's giantstep-babystep algorithm (or just bruteforce) to find the Diffie-Hellman shared secret
* use shared secret as key for hmac-md5. Message = (sequence number || bit)
* the resulting hash will match for either 0 or 1. Remember this bit.
* once you have all the right bits, binary -> hex gives you the flag

<h2> Challenge </h2>

My friend Ron is a grain farmer, and was going on and on about this messaging scheme he learned about that promises "confidentiality without encryption" (whatever that means). He gave me these files to try it out with and asked if I could "separate the wheat from the chaff". Shouldn't be too bad...

All numerical input to the HMAC should be treated as numbers, not strings. The message you will be HMACing is <bytes of sequence # || bytes of bit> where || is concatenation

Note: You must wrap the answer in utflag{} before submitting!


<h4> Included files </h4>

dh-for-hmac-md5.txt:

```
p = 78787
g = 16405

A = 59145
B = 18081
```

output.txt:
```
(0, 0, '369446d7ee79d762b2ddd13b5f551402')
(0, 1, '4e4cf903dfb170945de211e2d10e4d5a')
(1, 0, 'e47f829899064afd73c931fc2ddae635')
(1, 1, '4827e3d528cf2786288c906e98bd8ce7')
(2, 0, 'f97957ee05842e8d8cbacdfbfa917d7a')
(2, 1, 'e45858653a4769ab07fd4a9725cc4461')
(3, 0, 'fb126e02507ceed0a72cc14b93eaf8a4')
(3, 1, '889e4ecf756900a692be184d96379457')
(4, 0, 'c41591c7ec02ea3ba8809abd735ed0d8')
... 
(182, 1, '4a66c5d1b6170f1167f220032ed5a1ac')
(183, 0, 'cbb205db98295f7f9d75e21aabf46501')
(183, 1, '37fcff5c315f44b128dfba4247e995f5')
```

<h2> Solution </h2>

The name of the file is dh-for-hmac-md5.txt, and it contains the prime number (p), generator (g), first public key (A) and second public key (B). 

The private key a is such that `g^a = A`. Identifying it requires solving the discrete log problem. Fortunately, the values are all very small, so we can either bruteforce it, or use [Shanks' Baby-step giant-step algorithm](https://en.wikipedia.org/wiki/Baby-step_giant-step). I happened to have developed an implementation of the latter just a few days ago for cryptohack.org, so I decided to use it.

Once you have one of the private keys, calculating the shared secret is as easy as calling `pow(B, a, p)`.

(as a side note, this is also the solution to the _Small P Problems_ challenge in the crypto category of this CTF)

***

Now that we have the shared secret, we can use it for hmac-md5. The challenge description gives us the message:

```
All numerical input to the HMAC should be treated as numbers, not strings. The message you will be HMACing is <bytes of sequence # || bytes of bit> where || is concatenation
```

We can use pycryptodome's long_to_bytes() function to handle data wrangling, and we just concatenate the bytes. If we do this right, then either the either `HMAC-MD5(long_to_bytes(0) + long_to_bytes(0)) == '369446d7ee79d762b2ddd13b5f551402'` or `HMAC-MD5(long_to_bytes(0) + long_to_bytes(1)) == '4e4cf903dfb170945de211e2d10e4d5a'`. 

We jolt down the bit that got a match, then repeat for sequence #2, #3, ..., #183. 

At the end, we have 183 bits in our hands:

```
0110001101110010011001010110000101101101010111110110111101100110010111110111010001101000011001010101111101100011011100100110111101110000010111110011001000111001001100000011011100110100
```

When it comes to changing bases, cyberchef's got your back:


![cyberchef <3>](<./IMAGES/cyberchef.PNG>)

Final flag: `utflag{cream_of_the_crop_29074}`



