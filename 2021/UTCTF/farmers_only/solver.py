#!/usr/bin/env python3

import hmac
#https://pypi.org/project/serenest/
from serenest.dlputils import bsgs
import serenest.data_wrangling as dw
from Crypto.Util.number import long_to_bytes
from binascii import hexlify

p = 78787
g = 16405
A = 59145
B = 18081

"""
honestly, you could just brute force this by trying every value a from 1 to p
such that g^a = A.
"""
a = bsgs(g, A, p)
shared_secret = pow(B, a, p)


#now we got a key for our HMAC-MD5! Time to use it.
with open("output.txt", 'r') as f:
    lines = f.readlines()

#binary_string will remember all the 0s and 1s we saw so far
binary_string = ""

#an earlier version was verifying every single entry.
#once I got confident, I just assumed that if it didn't match bit 0, it would match bit 1
for x in range(len(lines)//2):
    my_hmac = hmac.new(long_to_bytes(shared_secret), None, 'md5')
    possible_hash_1 = lines[2*x].split("'")[1]
    my_hmac.update(long_to_bytes(x)+long_to_bytes(0))
    actual_hex = hexlify(my_hmac.digest())
    as_string = dw.bytes_to_string(actual_hex)
    
    if (as_string == possible_hash_1):
        binary_string += '0'
    else:
        binary_string += '1'

print(binary_string)

